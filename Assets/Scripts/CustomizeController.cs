﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using SimpleJSON;

[ExecuteInEditMode]
public class CustomizeController : MonoBehaviour
{
    [Header("Sprite Parts")]
    public SpriteRenderer[] spritePart;

    [Header("Sprite Options")]
    public Sprite[] bodyOptions;
    public Sprite[] mouthOptions;
    public Sprite[] upperArmOptions;
    public Sprite[] foreArmOptions;
    public Sprite[] handOptions;
    
    [Space]
    public int[] index;
    private int[] ex_index;

    [Space]
    [SerializeField] private bool isEdit = false;
    
    public void Customize(string part , int indexpart)
    {
        if(part == PunSkinSetting.PLAYER_BODY)
        {
            index[0] = indexpart;
        }
    }

    private void Update()
    {
        ex_index = new int[spritePart.Length];
        if (index.Length != ex_index.Length)
        {
            index = new int[spritePart.Length];
        }

        ApplyPart();
    }
    
    public void ApplyPart()
    {
        for (int i = 0; i < spritePart.Length; i++)
        {
            spritePart[i].sprite = ConvertIndexToSprite(i, index[i]);
        }
    }

    public Sprite ConvertIndexToSprite(int part, int index)
    {
        switch (part)
        {
            case 0: return bodyOptions[index]; break;
            case 1: return mouthOptions[index]; break;
            case 2: return upperArmOptions[index]; break;
            case 3: return foreArmOptions[index]; break;
            case 4: return handOptions[index]; break;
        }
        

        return null;
    }
    
    private int ConvertIndexToOption(int part)
    {
        switch (part)
        {
            case 0: return bodyOptions.Length; break;
            case 1: return mouthOptions.Length; break;
            case 2: return upperArmOptions.Length; break;
            case 3: return foreArmOptions.Length; break;
            case 4: return handOptions.Length; break;
        }

        return 0;
    }

    #region Obsoleted

    //Obsoleted CustomizeUpdate
    /*//Only run when customize character
    private void CustomizeUpdate() 
    {
        
        for (int i = 0; i < hairOptions.Length; i++)
        {
            if (i == index[_partTemp])
            {
                //spritePart[_partTemp].sprite = hairOptions[i];
                switch (_partTemp)
                {
                    case 0: spritePart[0].sprite = hairOptions[i]; break;//Hair
                    case 1: spritePart[1].sprite = bodyOptions[i]; break;//Body
                }
            }
        }
        //Body
        /*for (int i = 0; i < bodyOptions.Length; i++)
        {
            if (i == index[1])
            {
                bodyPart.sprite = bodyOptions[i];
            }
        }
    }*/
    //Obsoleted NextPart
    /*public void OldNextPart(int part)
    {
        isEdit = true;
        index[part]++;
        
        if (index[part] >= hairOptions.Length)
        {
            index[part] = 0;
        }
        //_partTemp = part;
        ApplyPart();
    }*/


    #endregion
    
    public void NextPart(int part)
    {
        isEdit = true;
        index[part]++;
        
        if (index[part] >= ConvertIndexToOption(part))
        {
            index[part] = 0;
        }
        
        ApplyPart();
    }
    
    public void PreviousPart(int part)
    {
        isEdit = true;
        index[part]--;
        
        if (index[part] < 0)
        {
            index[part] = ConvertIndexToOption(part) - 1;
        }
        //_partTemp = part;
        ApplyPart();
    }
    
    
    public void Save()
    {
        JSONObject playerAppearanceJson = new JSONObject();
        playerAppearanceJson.Add("Body",index[0]);
        playerAppearanceJson.Add("Mouth",index[1]);
        playerAppearanceJson.Add("UpperArm",index[2]);
        playerAppearanceJson.Add("ForeArm",index[3]);
        playerAppearanceJson.Add("Hand",index[4]);

        
        Debug.Log(playerAppearanceJson.ToString());
        //Save Json
        string path = Directory.GetCurrentDirectory() + "Assets/PlayerData/";
        if (!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);
        }
        File.WriteAllText(path + "PlayerAppearance.json", playerAppearanceJson.ToString());
    }

    public void Load()
    {
        isEdit = false;
        string path = Directory.GetCurrentDirectory() + "Assets/PlayerData/PlayerAppearance.json";
        string jsonString = File.ReadAllText(path);
        JSONObject playerAppearanceJson = (JSONObject)JSON.Parse(jsonString);
        spritePart[0].sprite = bodyOptions[playerAppearanceJson["Body"]]; index[0] = playerAppearanceJson["Body"];
        spritePart[1].sprite = mouthOptions[playerAppearanceJson["Mouth"]]; index[1] = playerAppearanceJson["Mouth"];
        spritePart[2].sprite = upperArmOptions[playerAppearanceJson["UpperArm"]]; index[2] = playerAppearanceJson["UpperArm"];
        spritePart[3].sprite = foreArmOptions[playerAppearanceJson["ForeArm"]]; index[3] = playerAppearanceJson["ForeArm"];
        spritePart[4].sprite = handOptions[playerAppearanceJson["Hand"]]; index[3] = playerAppearanceJson["Hand"];
    }
}
