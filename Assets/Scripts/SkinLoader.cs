﻿using System;
using System.IO;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using SimpleJSON;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class SkinLoader : MonoBehaviourPunCallbacks, IPunInstantiateMagicCallback
{
    private Player _player;
    
    [Header("Sprite Parts")]
    public SpriteRenderer[] spritePart;

    [Header("Sprite Options")]
    public Sprite[] bodyOptions;
    public Sprite[] mouthOptions;
    public Sprite[] upperArmOptions;
    public Sprite[] foreArmOptions;
    public Sprite[] handOptions;
    
    public int[] index;

    [SerializeField] private bool newPlayerJoin = false;
    
    public void OnPhotonInstantiate(PhotonMessageInfo info)
    {
        if (photonView.IsMine)
        {
            GetSkinIndex();
        }
    }

    private void Start()
    {
        ChangeSkinProperties();
    }

    /*
    public void UpdateSprite()
    {
        if (!photonView.IsMine)
            return;
        string path = Directory.GetCurrentDirectory() + "Assets/PlayerData/PlayerAppearance.json";
        string jsonString = File.ReadAllText(path);
        JSONObject playerAppearanceJson = (JSONObject)JSON.Parse(jsonString);
        hairPart.sprite = hairOptions[playerAppearanceJson["Hair"]]; index[0] = playerAppearanceJson["Hair"];
        bodyPart.sprite = bodyOptions[playerAppearanceJson["Body"]]; index[1] = playerAppearanceJson["Body"];
    }*/
    public void GetSkinIndex()
    {
        string path = Directory.GetCurrentDirectory() + "Assets/PlayerData/PlayerAppearance.json";
        string jsonString = File.ReadAllText(path);
        JSONObject playerAppearanceJson = (JSONObject)JSON.Parse(jsonString);
        index[0] = playerAppearanceJson["Body"];
        index[1] = playerAppearanceJson["Mouth"];
        index[2] = playerAppearanceJson["UpperArm"];
        index[3] = playerAppearanceJson["ForeArm"];
        index[4] = playerAppearanceJson["Hand"];
        Debug.Log("Body: " + index[0] + "/n" 
        + "Mouth: " + index[1] + "/n"
        + "UpperArm: " + index[2] + "/n"
        + "ForeArm: " + index[3] + "/n"
        + "Hand: " + index[4] + "/n"
        );
    }

    private void ChangeSkinProperties()
    {
        if (photonView.IsMine)
        {
            Hashtable skins = new Hashtable
            {
                {PunSkinSetting.PLAYER_BODY, index[0]},
                {PunSkinSetting.PLAYER_MOUTH, index[1]},
                {PunSkinSetting.PLAYER_UPPERARM, index[2]},
                {PunSkinSetting.PLAYER_FOREARM, index[3]},
                {PunSkinSetting.PLAYER_HAND, index[4]}
            };
            PhotonNetwork.LocalPlayer.SetCustomProperties(skins);
            for (int i = 0; i < spritePart.Length; i++)
            {
                spritePart[i].sprite = ConvertIndexToSprite(i, index[i]);
            }
        }
        else
        {
            OnPlayerPropertiesUpdate(photonView.Owner, photonView.Owner.CustomProperties);
            //_player = photonView.Owner;
            //spritePart[0].sprite = bodyOptions[(int)_player.CustomProperties[PunSkinSetting.PLAYER_BODY]];
            Debug.Log(_player.CustomProperties[PunSkinSetting.PLAYER_BODY].ToString());
        }
    }
    
    public Sprite ConvertIndexToSprite(int part, int index)
    {
        switch (part)
        {
            case 0: return bodyOptions[index]; break;
            case 1: return mouthOptions[index]; break;
            case 2: return upperArmOptions[index]; break;
            case 3: return foreArmOptions[index]; break;
            case 4: return handOptions[index]; break;
        }
        //Debug.Log("Convert: " + index);

        return null;
    }
    Sprite ConvertPropsToSprite(int part, Hashtable props)
    {
        switch (part)
        {
            case 0: return bodyOptions[(int) props[PunSkinSetting.PLAYER_BODY]]; break;
            case 1: return mouthOptions[(int) props[PunSkinSetting.PLAYER_MOUTH]]; break;
            case 2: return upperArmOptions[(int) props[PunSkinSetting.PLAYER_UPPERARM]]; break;
            case 3: return foreArmOptions[(int) props[PunSkinSetting.PLAYER_FOREARM]]; break;
            case 4: return handOptions[(int) props[PunSkinSetting.PLAYER_HAND]]; break;
        }
        return null;
    }
    
    public override void OnPlayerPropertiesUpdate(Player targetPlayer, Hashtable changedProps)
    {
        base.OnPlayerPropertiesUpdate(targetPlayer, changedProps);
        if (targetPlayer.ActorNumber == photonView.ControllerActorNr)
        {
            for (int i = 0; i < spritePart.Length; i++)
            {
                spritePart[i].sprite = ConvertPropsToSprite(i, changedProps);
            }
        }
        return;
    }
}
