﻿using UnityEngine;
using Photon.Pun;
using Photon.Pun.UtilityScripts;
using Photon.Realtime;

public class PunNetworkManager : ConnectAndJoinRandom 
{
    public static PunNetworkManager singleton;

    [Header("Spawn Info")]
    [Tooltip("The prefab to use for representing the player")]
    public GameObject GamePlayerPrefab;
    bool isGameStart = false;
    bool isFirstSetting = false;
    
    private void Awake()
    {
        singleton = this;
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        base.OnPlayerEnteredRoom(newPlayer);
        
        Debug.Log("New Player. " + newPlayer.ToString());
        
    }

    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();

        Camera.main.gameObject.SetActive(false);

        if (PunUserNetControl.LocalPlayerInstance == null)
        {
            Debug.Log("We are Instantiating LocalPlayer from " + SceneManagerHelper.ActiveSceneName);
            PunNetworkManager.singleton.SpawnPlayer();
        }
        else
        {
            Debug.Log("Ignoring scene load for " + SceneManagerHelper.ActiveSceneName);
        }

        //PhotonNetwork.CurrentRoom.CustomProperties
        //PhotonNetwork.CurrentRoom.Players[0].CustomProperties
    }

    public void SpawnPlayer()
    {
        // we're in a room. spawn a character for the local player. it gets synced by using PhotonNetwork.Instantiate
        PhotonNetwork.Instantiate(GamePlayerPrefab.name, 
                                    new Vector3(0f, 5f, 0f), Quaternion.identity, 0);
        //SkinLoader playerSkin = GamePlayerPrefab.GetComponent<SkinLoader>();
        //playerSkin.UpdateSprite();
        
        isGameStart = true;
    }

    private void Update() {
        if(PhotonNetwork.IsMasterClient != true)
            return;
        if (isGameStart == true){
            //AirDropHealling();
        }
    }
    /*private void AirDropHealling(){
        if (isFirstSetting == false){
            isFirstSetting = true;
            int half = numberOfHealing / 2;
            for (int i = 0; i< half; i++){
                PhotonNetwork.InstantiateSceneObject(HealingPrefeb.name
                , AirDrop.RandomPosition(5f)
                , AirDrop.RandomRotation()
                ,0);
            }
            m_count = m_CountDownDropHeal;
        }
        else{ if (GameObject.FindGameObjectsWithTag("Healing").Length < numberOfHealing){
            m_count -= Time.deltaTime;

            if (m_count <=0){
                m_count = m_CountDownDropHeal;
                PhotonNetwork.Instantiate(HealingPrefeb.name
                , AirDrop.RandomPosition(10f)
                , AirDrop.RandomRotation()
                , 0);
            }
        }}
    }*/
}
