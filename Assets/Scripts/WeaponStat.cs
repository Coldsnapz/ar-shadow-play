﻿using System;
using UnityEngine;
using Photon.Pun;

[RequireComponent(typeof(PhotonTransformView))]
public class WeaponStat : MonoBehaviourPun, IPunObservable
{
    public Weapon weapon;
    
    public float attackForce = 20;
    public float atkSpd = 0.5f;
    public float attackDamage;
    public float attackRange;
    public bool isRange = false;
    public GameObject bullet;
    /*private void OnTriggerEnter(Collider other)
    {
        if (!photonView.IsMine)
            return;
        if (other.gameObject.CompareTag("Player"))
        {
            PunUserNetControl tempOther = other.gameObject.GetComponent<PunUserNetControl>();
            if (tempOther != null)
                Debug.Log("Hit Other ViewID : " + tempOther.photonView.ViewID);
            other.GetComponent<Rigidbody>().AddForce(transform.forward * force, ForceMode.Impulse);
        }
    }
    private void OnDestroy()
    {
        if (!photonView.IsMine)
            return;

        //PhotonView.Destroy(this.gameObject);
        PhotonNetwork.Destroy(this.gameObject);
    }*/

    private void Start()
    {
        ChangeWeapon();
    }
    

    public void ChangeWeapon()
    {
        attackForce = weapon.attackForce;
        atkSpd = weapon.attackSpd;
        attackDamage = weapon.attackDamage;
        attackRange = weapon.attackRange;
        isRange = weapon.isRange;
        bullet = weapon.bullet;

    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(attackForce);
            //stream.SendNext(attackDamage);
            //stream.SendNext(attackRange);
        }
        else
        {
            attackForce = (float)stream.ReceiveNext();
            //attackDamage = (float)stream.ReceiveNext();
            //attackRange = (float)stream.ReceiveNext();
        }
    }
}
