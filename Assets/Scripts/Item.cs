﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class Item : MonoBehaviourPun
{
    public Weapon weapon;
    private Sprite _sprite;
    [SerializeField]private SpriteRenderer image;

    private void Start()
    {
        _sprite = weapon.art;
        image.sprite = _sprite;
    }

    private void OnDestroy()
    {
        if (!photonView.IsMine)
            return;

        //PhotonView.Destroy(this.gameObject);
        PhotonNetwork.Destroy(this.gameObject);
    }
}
