using System;
using UnityEngine;
using UnityEngine.Serialization;
using Photon.Pun;
using Photon.Pun.Demo.Procedural;

public class CharacterController : MonoBehaviourPun
{
    
    [SerializeField] private float speed = 2f;

    [SerializeField] private Rigidbody rb = null;
    
    [Tooltip("Is Knockingback")]
    public bool unbalance = false;
    public bool isBlock = false;
    
    [Tooltip("Block WaitTime")]
    [SerializeField] private float waitTime = 0;
    public float blockInterval = 1.0f;
    private float _blockTime = 0.6f;

    [SerializeField] private WeaponStat weaponStat;
    [SerializeField] private BulletStat bulletStat;
    
    private Vector3 _movement;
    
    #region MonoBehaviour
    private void Update()
    {
        if (!photonView.IsMine)
            return;
        
        if (this.rb.velocity.x > speed || this.rb.velocity.z > speed ||
            this.rb.velocity.x < -speed || this.rb.velocity.z < -speed)
        {
            unbalance = true;
        }
        else
        {
            unbalance = false;
        }
        /*animator.SetBool (WALK_PROPERTY,
          Math.Abs (_movement.sqrMagnitude) > Mathf.Epsilon);*/
        ProcessInput();
        TimeCount();
    }

    private void ProcessInput()
    {
        //Get Input
        float inputX = 0;
        float inputY = 0;

        inputX = Input.GetAxisRaw("Horizontal");
        inputY = Input.GetAxisRaw("Vertical");
        // Normalize
        _movement = new Vector3(inputX, 0, inputY).normalized;

        //Block
        if (Input.GetKey(KeyCode.Space))
        {
            Block();
        }
    }

    private void FixedUpdate()
    {
        if (!unbalance)
            Move();
    }

    //Get Hit!
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Weapon") && !isBlock)
        {
            if (other.transform.root != transform.root)
            {
                weaponStat = other.GetComponent<WeaponStat>();
                unbalance = true;
                rb.AddForce(other.transform.forward * weaponStat.attackForce, ForceMode.Impulse);
            }
        }

        if (other.gameObject.CompareTag("Bullet") && !isBlock)
        {
            if (other.transform.root != transform.root)
            {
                bulletStat = other.GetComponent<BulletStat>();
                unbalance = true;
                rb.AddForce(other.transform.forward * bulletStat.attackForce, ForceMode.Impulse);
            }
        }
    }

    private void TimeCount() //Main Timer
    {
        if (waitTime > 0)
        {
            waitTime -= Time.deltaTime;
        }
        else
        {
            waitTime = 0;
        }

        if (isBlock)
        {
            _blockTime -= Time.deltaTime;
        }

        if (_blockTime <= 0)
        {
            Debug.Log("unblock");
            isBlock = false;
            _blockTime = 0.8f;
        }
    }

    //Block
    private void Block()
    {
        if (waitTime <= 0 && !isBlock)
        {
            Debug.Log("block");
            isBlock = true;
            waitTime = blockInterval;
        }
    }

    private void Move()
    {
        rb.velocity = new Vector3(_movement.x * speed, rb.velocity.y, _movement.z * speed);
    }

    #endregion
}