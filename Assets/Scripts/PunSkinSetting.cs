﻿using UnityEngine;

public class PunSkinSetting
{
    public const string PLAYER_BODY = "PlayerBody";
    public const string PLAYER_MOUTH = "PlayerMouth";
    public const string PLAYER_UPPERARM = "PlayerUpperArm";
    public const string PLAYER_FOREARM = "PlayerForeArm";
    public const string PLAYER_HAND = "PlayerHand";
}
